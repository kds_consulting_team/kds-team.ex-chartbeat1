FROM quay.io/keboola/docker-custom-python:latest

RUN pip3 install --upgrade git+git://github.com/keboola/python-docker-application.git@2.1.0
RUN pip3 install --upgrade https://bitbucket.org/kds_consulting_team/keboola-python-util-lib/get/0.2.0.zip#egg=kbc
COPY . /code/
WORKDIR /code/


CMD ["python", "-u", "/code/main.py"]
