import datetime
import logging
import os
import time

from kbc.client_base import HttpClientBase
from kbc.env_handler import KBCEnvHandler

SLEEP_TIME = 30

MAX_WAIT_TIME = 3600

MAX_RETRIES = 10
DEFAULT_REQ_TIMEOUT = 180

PAR_TABLE_NAME = 'table_name'

PAR_ENDDT = 'end'
PAR_STARTDT = 'start'
PAR_PKEY = 'primary_key'
PAR_SORT_ORDER = 'sort_order'
PAR_SORT_COLUMN = 'sort_column'
PAR_TZ = 'tz'
PAR_DIMENSIONS = 'dimensions'
PAR_USERID = 'user_id'
PAR_LIMIT = 'limit'
PAR_HOST = 'host'
PAR_FILTER = 'filter'
PAR_GROUP = 'group'
KEY_API_TOKEN = '#apikey'

PAR_METRICS = 'metrics'
# for backward compatibility
PAR_SUBDOMAIN = 'subdomain'

MANDATORY_PARS = [KEY_API_TOKEN, PAR_ENDDT, PAR_STARTDT, PAR_SORT_COLUMN, PAR_SORT_ORDER, PAR_TZ, PAR_HOST, PAR_LIMIT]
MANDATORY_IMAGE_PARS = []

APP_VERSION = '0.1.4'


class Component(KBCEnvHandler):

    def __init__(self, debug=False):
        KBCEnvHandler.__init__(self, MANDATORY_PARS)
        # override debug from config
        if self.cfg_params.get('debug'):
            debug = True

        self.set_default_logger('DEBUG' if debug else 'INFO')
        logging.info('Running version %s', APP_VERSION)
        logging.info('Loading configuration...')

        try:
            self.validate_config()
            self.validate_image_parameters(MANDATORY_IMAGE_PARS)
        except ValueError as e:
            logging.error(e)
            exit(1)

    def run(self, debug=False):
        '''
        Main execution code
        '''
        params = self.cfg_params  # noqa
        client = HttpClientBase('http://api.chartbeat.com/query/v2/',
                                status_forcelist=(400, 402, 403, 404, 405, 500, 502, 503, 504, 598),
                                default_params={'apikey': params[KEY_API_TOKEN]}, max_retries=MAX_RETRIES)

        start = int(params[PAR_STARTDT])
        today = datetime.datetime.now()
        dd_start = datetime.timedelta(days=start)
        earlier_start = today + dd_start
        earlier_str_start = earlier_start.strftime("%Y-%m-%d")

        end = int(params[PAR_ENDDT])
        dd_end = datetime.timedelta(days=end)
        earlier_end = today + dd_end
        earlier_str_end = earlier_end.strftime("%Y-%m-%d")

        filters = params.get(PAR_FILTER, {})

        req_pars = {
            'host': params[PAR_HOST],
            'user_id': params[PAR_USERID],
            'limit': params[PAR_LIMIT],
            'start': earlier_str_start,
            'end': earlier_str_end,
            'metrics': params[PAR_METRICS],
            # backward compatibility
            'subdomain': params.get(PAR_SUBDOMAIN),
            'dimensions': params[PAR_DIMENSIONS],
            'tz': params[PAR_TZ],
            'sort_column': params[PAR_SORT_COLUMN],
            'sort_order': params[PAR_SORT_ORDER],
            'primary_key': params[PAR_PKEY]
        }

        for f in filters:
            if f['filter'] == PAR_SUBDOMAIN:
                # use filter instead
                req_pars.pop('subdomain')
            f_dict = {f['filter']: f['value']}
            req_pars = {**req_pars, **f_dict}

        t0 = time.time()
        result_of_first_api_call = {}
        logging.info('Running query %s', req_pars)
        try:
            result_of_first_api_call = client.get(
                'http://api.chartbeat.com/query/v2/submit/page/?', params=req_pars,
                timeout=DEFAULT_REQ_TIMEOUT
            )
        except Exception as x:
            logging.error('Api call failed with error %s', x)
            exit(1)
        t1 = time.time()
        logging.info('Query Took %s seconds', str(t1 - t0))

        # generate query_id as a result of first API call
        query_id = str(result_of_first_api_call['query_id'])
        logging.info("query_id: %s", query_id)

        self._wait_until_report_ready(client, params[PAR_HOST], query_id)
        second_api_call = self._fetch_query_result(client, params[PAR_HOST], query_id)

        # result of second API call as text object
        result_of_second_api_call = second_api_call.text
        if result_of_second_api_call.startswith('Error'):
            logging.error('No data were fetched: %s', result_of_second_api_call)
            exit(1)
        # result of second API call as CSV
        # save query_id to data/out/state.json {'query_id': query_id}

        self._store_result(result_of_second_api_call, params[PAR_TABLE_NAME], params[PAR_PKEY])

        logging.info('Extraction finished successfully!')

    def _store_result(self, result, name, pk):
        out_full_name = os.path.join(self.tables_out_path, name + '.csv')
        out_destination = ''
        file = open(out_full_name, 'w', newline='', encoding='utf-8')
        file.write(result)
        file.close()

        # write manifest
        self.configuration.write_table_manifest(out_full_name, destination=out_destination, primary_key=pk,
                                                incremental=True)

    def _fetch_query_result(self, client, host, query_id):
        # else part starts here
        # second API call, repeats every 180 sec
        pa1 = {'host': host, 'query_id': query_id}

        t0 = time.time()
        try:
            second_api_call = client.get_raw(
                'http://api.chartbeat.com/query/v2/fetch/?', params=pa1,
                timeout=DEFAULT_REQ_TIMEOUT
            )

        except Exception as x:
            logging.error(x)
            exit(1)

        t1 = time.time()
        logging.info('Query Took %s seconds', str(t1 - t0))

        return second_api_call

    def _wait_until_report_ready(self, client, host, query_id):
        pa1 = {'host': host, 'query_id': query_id}

        t0 = time.time()
        is_completed = False
        is_timed_out = False

        while not is_completed and not is_timed_out:
            try:
                time.sleep(SLEEP_TIME)
                status_res = client.get(
                    'http://api.chartbeat.com/query/v2/status/?', params=pa1,
                    timeout=DEFAULT_REQ_TIMEOUT
                )

            except Exception as x:
                logging.error(x)
                exit(1)

            logging.debug('Report status %s', status_res.get('status'))
            if status_res.get('status') in ['completed', 'downloaded']:
                is_completed = True
            if (time.time() - t0) > MAX_WAIT_TIME:
                is_timed_out = True

        t1 = time.time()
        logging.info('Query Took %s seconds', str(t1 - t0))


"""
        Main entrypoint
"""
if __name__ == "__main__":
    comp = Component()
    comp.run()
